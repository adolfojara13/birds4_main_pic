#include <18F67J94.h>
#fuses NOWDT, NOBROWNOUT, NOPROTECT                                              //Fusibles: Sin WatchDog Timer, sin brownout, sin proteccion de memoria de programa
#fuses FRC_PLL, PLL1, DEBUG, NODS 
#include <PIC18F67J94_registers.h>
#device ADC=16
#use delay(crystal=16MHz,clock=16MHz)                                            //clock=indica al compilador el valor del clock del procesador y el tipo de clock

#use rs232(baud=9600,parity=N,xmit=PIN_B6,rcv=PIN_B7,bits=8,stream=PORT1)        //Configuracion rs232, PORT1
//#use rs232(baud=9600,parity=N,xmit=PIN_D4,rcv=PIN_D5,bits=8,stream=MPIC)         //Configuracion rs232, MPIC

int8 sec = 0;                                                                    //sec de 0 255
int8 min = 0;
int8 hou = 0;
unsigned int16 day_num = 0;                                                      //day_num de 0 a 65535
int16 main_count = 0;                                                            //contador principal


#INT_TIMER1                                                                      //Funcion de interrupcion del TMR1
void TIMER1_isr() {                                                              //Se utiliza para generar hhmmss
      set_timer1(0x8000);                                                        //Carga del timer1 para interrupcion cada 1 segundo
      main_count++;                                                              //Incrementa el contador principal

      if (sec < 59){
      sec++;
      }else{
         sec = 0;
         min++;
      }
      if (min == 60){
         min = 0;
         hou++;
      }
      
      if (hou == 24){                                                            //Si se cumple 24hs
         hou = 0;                                                                //Resetea la variable hora
         day_num++;                                                              //Incrementa la variable dia
         
      }
      
      fprintf(PORT1,"%02ld", day_num);                                                  //Se imprime ddhhmmss
      fprintf(PORT1,":%02d", hou);
      fprintf(PORT1,":%02d", min);
      fprintf(PORT1,":%02d\r\n", sec);
      fprintf(PORT1,"maincount:%04ld\r\n", main_count);                                 //Imprime contador principal

}

void main()
{

 /*                                                                                //Configuracion de pines In/Out
   TRISC2 = 0; RC2 = 0;                                                          //Unreg#1 Enable (OCP 4A)                                                        
   TRISC3 = 0; RC3 = 0;                                                          //5V0 Enable (OCP 2A)
   TRISC4 = 0; RC4 = 0;                                                          //3V3#1 ON (converter)
   TRISC5 = 0; RC5 = 0;                                                          //Unreg#2 Enable (OCP 3A)
   
   TRISD0 = 0; RD0 = 0;                                                          //3V3#2 Enable (OCP 2A)
   TRISD1 = 0; RD1 = 0;                                                          //3V3#1 Enable (OCP 2A)
   TRISD2 = 0; RD2 = 0;                                                          //5V0 ON (converter)
   TRISD3 = 0; RD3 = 0;                                                          //3V3#2 ON (converter)
   TRISD6 = 0; RD6 = 0;                                                          //COM PIC Enable
   TRISD7 = 0; RD7 = 0;                                                          //MAIN PIC Enable
   
   TRISB2 = 0; RB2 = 0;                                                          //Al WDT externo
   TRISB5 = 0; RB5 = 1;                                                          //RAW voltage monitor ON   

   clear_interrupt(int_timer1);
   setup_timer_1(T1_EXTERNAL | T1_DIV_BY_1);                                     //Config TMR1, clock externo, prescaler=1
   set_timer1(0x8000);                                                           //Carga del timer1 para interrupcion cada 1 segundo
   //T1OSCEN = 1;                                                                  //Oscilador del TMR1 habilitado
   enable_interrupts(INT_TIMER1);                                                //Habilitacion de interrupcion TMR1
   enable_interrupts(GLOBAL);                                                    //Habilitacion de interrupciones Globales
*/
   printf("*********************************************************\n\r");
   printf("********    BIRDS4 MAIN PIC BBM VERSION      ***********\n\r");
   printf("*********************************************************\n\r");

   while(TRUE)
   {
   printf("********    RUNNIG      ***********\n\r");
   delay_ms(1000);

      //TODO: User Code
   }

}
